package br.com.ioasys.empresas.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.ioasys.empresas.services.EnterpriseService;

@RestController
@RequestMapping(value = "/api/v1/enterprises")
public class EnterpriseResources {

	@Autowired
	private EnterpriseService enterpriseService;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> findById(@PathVariable Integer id) {
		return ResponseEntity.ok(enterpriseService.findByIdService(id));
	}

	@GetMapping
	public ResponseEntity<?> filterByType(@RequestParam(value = "enterprise_types", required = false) Integer enterprise_types,
			@RequestParam(value = "name", required = false) String name) {
		return ResponseEntity.ok(enterpriseService.findByIdName(enterprise_types, name));
	}

}
