package br.com.ioasys.empresas.domain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Enterprises {

	@JsonProperty("enterprises")
	private List<EnterpriseOnly> enterprises;

	public List<EnterpriseOnly> getEnterprises() {
		return enterprises;
	}

	public void setEnterprises(List<EnterpriseOnly> enterprises) {
		this.enterprises = enterprises;
	}

	public Enterprises(List<EnterpriseOnly> enterprises) {
		super();
		this.enterprises = enterprises;
	}

}