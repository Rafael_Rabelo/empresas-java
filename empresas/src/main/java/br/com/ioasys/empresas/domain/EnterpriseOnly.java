package br.com.ioasys.empresas.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonProperty;


@Entity(name = "enterprise")
public class EnterpriseOnly implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonProperty("id")
	private Integer id;
	
	@JsonProperty("enterprise_name")
	private String enterpriseName;
	
	@JsonProperty("description")
	private String description;
	
	@JsonProperty("email_enterprise")
	private String emailEnterprise;

	@JsonProperty("facebook")
	private String facebook;

	@JsonProperty("twitter")
	private String twitter;

	@JsonProperty("linkedin")
	private String linkedin;

	@JsonProperty("phone")
	private String phone;
	
	@JsonProperty("own_enterprise")
	private boolean ownEnterprise;

	@JsonProperty("photo")
	private String photo;

	@JsonProperty("value")
	private int value;

	@JsonProperty("shares")
	private int shares;
	
	@JsonProperty("share_price")
	private double sharePrice;
	
	@JsonProperty("own_shares")
	private int ownShares;

	@JsonProperty("city")
	private String city;
	
	@JsonProperty("country")
	private String country;
	
	@JsonProperty("enterprise_type")
	@ManyToOne
	private EnterpriseType enterpriseType;
	
	public EnterpriseOnly() {

	}

	public EnterpriseOnly(Integer id, String emailEnterprise, String facebook, String twitter, String linkedin,
			String phone, boolean ownEnterprise, String enterpriseName, String photo, String description, String city,
			String country, int value, double sharePrice, EnterpriseType enterpriseType) {
		super();
		this.id = id;
		this.emailEnterprise = emailEnterprise;
		this.facebook = facebook;
		this.twitter = twitter;
		this.linkedin = linkedin;
		this.phone = phone;
		this.ownEnterprise = ownEnterprise;
		this.enterpriseName = enterpriseName;
		this.photo = photo;
		this.description = description;
		this.city = city;
		this.country = country;
		this.value = value;
		this.sharePrice = sharePrice;
		this.enterpriseType = enterpriseType;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmailEnterprise() {
		return emailEnterprise;
	}

	public void setEmailEnterprise(String emailEnterprise) {
		this.emailEnterprise = emailEnterprise;
	}

	public String getFacebook() {
		return facebook;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	public String getTwitter() {
		return twitter;
	}

	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}

	public String getLinkedin() {
		return linkedin;
	}

	public void setLinkedin(String linkedin) {
		this.linkedin = linkedin;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public boolean isOwnEnterprise() {
		return ownEnterprise;
	}

	public void setOwnEnterprise(boolean ownEnterprise) {
		this.ownEnterprise = ownEnterprise;
	}

	public String getEnterpriseName() {
		return enterpriseName;
	}

	public void setEnterpriseName(String enterpriseName) {
		this.enterpriseName = enterpriseName;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public int getShares() {
		return shares;
	}

	public void setShares(int shares) {
		this.shares = shares;
	}
	
	public double getSharePrice() {
		return sharePrice;
	}

	public void setSharePrice(double sharePrice) {
		this.sharePrice = sharePrice;
	}

	public EnterpriseType getEnterpriseType() {
		return enterpriseType;
	}

	public void setEnterpriseType(EnterpriseType enterpriseType) {
		this.enterpriseType = enterpriseType;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EnterpriseOnly other = (EnterpriseOnly) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
