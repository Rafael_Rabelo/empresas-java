package br.com.ioasys.empresas.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "enterprise", "success" })
public class Enterprise {

	@JsonProperty("enterprise")
	private EnterpriseOnly enterprise;
	@JsonProperty("success")
	private boolean success;

	public EnterpriseOnly getEnterprise() {
		return enterprise;
	}

	public void setEnterprise(EnterpriseOnly enterprise) {
		this.enterprise = enterprise;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

}