package br.com.ioasys.empresas.security;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class AuthenticationExceptionHandler implements AuthenticationEntryPoint, Serializable {

	private static final long serialVersionUID = 1L;

	@Override
	public void commence(HttpServletRequest req, HttpServletResponse res,
			AuthenticationException authException) throws IOException, ServletException {
		
		SecurityContextHolder.clearContext();
		Response responseMsg = new Response();
		res.setHeader("Status", "401 Unauthorized");
		responseMsg.setSuccess(null);
		List<String> errors = new ArrayList<String>();
		ObjectMapper mapper = new ObjectMapper();
		res.setStatus(HttpStatus.UNAUTHORIZED.value());
		res.setContentType(MediaType.APPLICATION_JSON_VALUE);
		errors.add("You need to sign in or sign up before continuing.");
		responseMsg.setErrors(errors);
		mapper.writeValue(res.getWriter(), responseMsg);
	}

}
