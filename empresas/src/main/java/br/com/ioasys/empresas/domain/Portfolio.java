package br.com.ioasys.empresas.domain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;


public class Portfolio {

	@JsonProperty("enterprises_number")
	private int enterprisesNumber;
	
	@JsonProperty("enterprises")
	private List<EnterpriseOnly> enterprises = null;

	public int getEnterprisesNumber() {
		return enterprisesNumber;
	}

	public void setEnterprisesNumber(int enterprisesNumber) {
		this.enterprisesNumber = enterprisesNumber;
	}

	public List<EnterpriseOnly> getEnterprises() {
		return enterprises;
	}

	public void setEnterprises(List<EnterpriseOnly> enterprises) {
		this.enterprises = enterprises;
	}

}
