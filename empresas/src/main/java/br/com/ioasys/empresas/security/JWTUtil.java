package br.com.ioasys.empresas.security;

import java.util.Date;

import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JWTUtil {

	static final String SECRET = "random_secret_key ";
	
	public String generateToken(String username) {
		return Jwts.builder()
				.setSubject(username)
				.setExpiration(new Date(System.currentTimeMillis() + (100000 * 120)))
				.signWith(SignatureAlgorithm.HS512, SECRET.getBytes())
				.compact();
	}
	
	public boolean validToken(String token) {
		Claims claims = getClaims(token);
		if(claims != null) {
			String username = claims.getSubject();
			Date expDate = claims.getExpiration();
			Date now = new Date(System.currentTimeMillis());
			if(username != null && expDate != null && now.before(expDate)) {
				return true;
			}
		}
		return false;
	}
	
	public String getUsername(String token) {
		Claims claims = getClaims(token);
		if(claims != null) {
			return claims.getSubject();
		}
		return null;
	}
	private Claims getClaims(String token) {
		try {
			return Jwts.parser().setSigningKey(SECRET.getBytes()).parseClaimsJws(token).getBody();
		} 
		catch (Exception e) {
			return null;
		}
		
	}
	
}
