package br.com.ioasys.empresas.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.ioasys.empresas.domain.Investor;

@Repository
public interface InvestorRepository extends JpaRepository<Investor, Integer> {
	Optional<Investor> findByEmail(String email);
}
