package br.com.ioasys.empresas.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ioasys.empresas.domain.Enterprise;
import br.com.ioasys.empresas.domain.EnterpriseOnly;
import br.com.ioasys.empresas.domain.EnterpriseType;
import br.com.ioasys.empresas.domain.Enterprises;
import br.com.ioasys.empresas.repositories.EnterpriseRepository;

@Service
public class EnterpriseService {

	@Autowired
	private EnterpriseRepository enterpriseRepository;

	public List<EnterpriseOnly> findAllService() {
		return enterpriseRepository.findAll();
	}

	public Enterprise findByIdService(Integer id) {
		Enterprise enterprise = new Enterprise();
		Optional<EnterpriseOnly> optEnterprise = enterpriseRepository.findById(id);
		enterprise.setEnterprise(optEnterprise.get());
		enterprise.setSuccess(optEnterprise.isPresent());
		return enterprise;
	}

	public Enterprises findByIdName(Integer id, String name) {
		ArrayList<EnterpriseOnly> enterprises = new ArrayList<EnterpriseOnly>();
		if (id != null && name != null) {
			EnterpriseType enterpriseType = new EnterpriseType();
			enterpriseType.setId(id);
			enterpriseType.setName(name);
			enterprises = enterpriseRepository.findByEnterpriseTypeIdAndEnterpriseName(id, name);
		} else if (id == null) {
			enterprises = enterpriseRepository.findByEnterpriseName(name);
		} else if (name == null) {
			enterprises = enterpriseRepository.findByEnterpriseTypeId(id);
		}
		return new Enterprises(enterprises);
	}

}
