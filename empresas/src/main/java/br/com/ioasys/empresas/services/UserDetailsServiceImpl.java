package br.com.ioasys.empresas.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.ioasys.empresas.domain.Investor;
import br.com.ioasys.empresas.repositories.InvestorRepository;
import br.com.ioasys.empresas.security.UserSS;

@Service
public class UserDetailsServiceImpl implements UserDetailsService{
	
	@Autowired
	private InvestorRepository investorRepository;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Investor investor = findByEmail(email);
		return new UserSS(investor.getId(),  investor.getEmail(), "{noop}" + investor.getPassword(), null);
	}
	
	public Investor findByEmail(String email) {
		Optional<Investor> optInvestor = investorRepository.findByEmail(email);
		if(optInvestor.isPresent()) {
			return optInvestor.get();
		}else {
			throw new UsernameNotFoundException(email);
		}
	}

}
