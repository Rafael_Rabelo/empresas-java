package br.com.ioasys.empresas.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "investor", "enterprise", "success" })
public class User {

	@JsonProperty("investor")
	private Investor investor;
	
	@JsonProperty("enterprise")
	private EnterpriseOnly enterprise;
	
	@JsonProperty("success")
	private boolean success;
	
	@JsonProperty("investor")
	public Investor getInvestor() {
		return investor;
	}

	@JsonProperty("investor")
	public void setInvestor(Investor investor) {
		this.investor = investor;
	}

	@JsonProperty("enterprise")
	public EnterpriseOnly getEnterprise() {
		return enterprise;
	}

	@JsonProperty("enterprise")
	public void setEnterprise(EnterpriseOnly enterprise) {
		this.enterprise = enterprise;
	}

	@JsonProperty("success")
	public boolean isSuccess() {
		return success;
	}

	@JsonProperty("success")
	public void setSuccess(boolean success) {
		this.success = success;
	}

}
