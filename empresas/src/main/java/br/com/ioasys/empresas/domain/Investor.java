package br.com.ioasys.empresas.domain;


import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity(name = "investor")
public class Investor {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonProperty("id")
	private int id;
	
	@JsonProperty("investor_name")
	private String investorName;
	
	@JsonProperty("email")
	private String email;
	
	@JsonProperty("city")
	private String city;
	
	@JsonProperty("country")
	private String country;
	
	@JsonProperty("balance")
	private double balance;
	
	@JsonProperty("photo")
	private String photo;
	
	@JsonIgnore
	@ManyToMany
    @JoinTable(name="portfolio", joinColumns=
    {@JoinColumn(name="investor_id")}, inverseJoinColumns=
      {@JoinColumn(name="enterprise_id")})
	private List<EnterpriseOnly> enterprises;

	@Transient
	@JsonProperty("portfolio")
	private Portfolio portfolio;
	
	@JsonProperty("portfolio_value")
	private double portfolioValue;
	
	@JsonProperty("first_access")
	private boolean firstAccess;
	
	@JsonProperty("super_angel")
	private boolean superAngel;
	
	@JsonIgnore
	private String password;
	
	public Investor() {
	}
	
	public Investor(int id, String investorName, String email, String city, String country, double balance,
			String photo, Portfolio portfolio, double portfolioValue, boolean firstAccess, boolean superAngel,
			String password) {
		super();
		this.id = id;
		this.investorName = investorName;
		this.email = email;
		this.city = city;
		this.country = country;
		this.balance = balance;
		this.photo = photo;
		this.portfolio = portfolio;
		this.portfolioValue = portfolioValue;
		this.firstAccess = firstAccess;
		this.superAngel = superAngel;
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getInvestorName() {
		return investorName;
	}

	public void setInvestorName(String investorName) {
		this.investorName = investorName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public String getPhoto() {
		if(photo.equals("") && photo.trim().equals("")){
			photo = null;
		}
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Portfolio getPortfolio() {
		if(portfolio == null) {
			portfolio = new Portfolio();
		}
		portfolio.setEnterprises(getEnterprises());
		portfolio.setEnterprisesNumber(portfolio.getEnterprises().size());
		return portfolio;
	}

	public void setPortfolio(Portfolio portfolio) {
		this.portfolio = portfolio;
	}

	public double getPortfolioValue() {
		return portfolioValue;
	}

	public void setPortfolioValue(double portfolioValue) {
		this.portfolioValue = portfolioValue;
	}

	public boolean isFirstAccess() {
		return firstAccess;
	}

	public void setFirstAccess(boolean firstAccess) {
		this.firstAccess = firstAccess;
	}

	public boolean isSuperAngel() {
		return superAngel;
	}

	public void setSuperAngel(boolean superAngel) {
		this.superAngel = superAngel;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public List<EnterpriseOnly> getEnterprises() {
		return enterprises;
	}

	public void setEnterprises(List<EnterpriseOnly> enterprises) {
		this.enterprises = enterprises;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Investor other = (Investor) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
