package br.com.ioasys.empresas.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.ioasys.empresas.domain.Investor;
import br.com.ioasys.empresas.domain.User;
import br.com.ioasys.empresas.services.UserDetailsServiceImpl;

@RestController
@RequestMapping(value = "/api/v1/users")
public class UsersResources {

	@Autowired
	private UserDetailsServiceImpl UserDetailsServiceImpl;
	
	@RequestMapping(value = "/auth/sign_in", method = RequestMethod.POST)
	public ResponseEntity<?> findById(@RequestAttribute("uid") String username) {
		if(username != null){
			Investor investor = UserDetailsServiceImpl.findByEmail(username);
			User user = new User();
			user.setInvestor(investor);
			user.setEnterprise(null);
			user.setSuccess(true);
			return ResponseEntity.ok(user);
		}
		return null;
	}

}
