package br.com.ioasys.empresas.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.ioasys.empresas.domain.Credenciais;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	static final String TOKEN_PREFIX = "Bearer ";

	static final String HEADER_STRING = "access-token";

	private AuthenticationManager authenticationManager;

	private JWTUtil jwtUtil;
	
	public JWTAuthenticationFilter(AuthenticationManager authenticationManager, JWTUtil jwtUtil) {
		super();
		this.authenticationManager = authenticationManager;
		this.jwtUtil = jwtUtil;
		setFilterProcessesUrl("/api/v1/users/auth/sign_in");
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res)
			throws AuthenticationException {
		try {
			Credenciais creds = new ObjectMapper().readValue(req.getInputStream(), Credenciais.class);
			UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(creds.getEmail(),
					creds.getPassword(), new ArrayList<>());
			Authentication auth = authenticationManager.authenticate(authToken);
			return auth;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res, FilterChain chain,
			Authentication auth) throws IOException, ServletException {
		String username = ((UserSS) auth.getPrincipal()).getUsername();
		String token = jwtUtil.generateToken(username);
		res.addHeader(HEADER_STRING, TOKEN_PREFIX + token);
		res.addHeader("token-type", "Bearer");
		res.addHeader("uid", username);
		res.setContentType("application/json");
		res.setCharacterEncoding("UTF-8");
		req.setAttribute("uid", username);
		chain.doFilter(req, res);
	}

	@Override
	protected void unsuccessfulAuthentication(HttpServletRequest req, HttpServletResponse res,
			AuthenticationException authException) throws IOException {
		
		SecurityContextHolder.clearContext();
		Response responseMsg = new Response();
		res.setHeader("Status", "401 Unauthorized");
		responseMsg.setSuccess(false);
		List<String> errors = new ArrayList<String>();
		ObjectMapper mapper = new ObjectMapper();
		res.setStatus(HttpStatus.UNAUTHORIZED.value());
		res.setContentType(MediaType.APPLICATION_JSON_VALUE);

		if (authException instanceof BadCredentialsException) {
			errors.add("Invalid login credentials. Please try again.");
		}else {
			errors.add("Authentication failed");
		}
		responseMsg.setErrors(errors);
		mapper.writeValue(res.getWriter(), responseMsg);
	}

}
