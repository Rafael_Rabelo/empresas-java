package br.com.ioasys.empresas.repositories;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.ioasys.empresas.domain.EnterpriseOnly;

@Repository
public interface EnterpriseRepository extends JpaRepository<EnterpriseOnly, Integer> {
	ArrayList<EnterpriseOnly> findByEnterpriseTypeIdAndEnterpriseName(Integer id, String name);

	ArrayList<EnterpriseOnly> findByEnterpriseName(String name);

	ArrayList<EnterpriseOnly> findByEnterpriseTypeId(Integer id);
}
